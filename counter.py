import cv2

class Counter:

    def __init__(self):
        pass

    def register_object(self, shrimp_dict, box_ids):

        for box_id in box_ids:
            x, y, w, h, id = box_id
            y_centroid = y + (h/2)
            if id not in shrimp_dict.keys():
                shrimp_dict[id] = [y_centroid]
            
            else:
                (shrimp_dict[id]).append(y_centroid)

        return shrimp_dict

    def countShrimps(self, shrimp_dict, thresh_line, count = 0, count_his = {}):

        for k in shrimp_dict.keys():
            if (shrimp_dict[k])[0] < thresh_line:
                if k not in count_his.keys():
                    if (shrimp_dict[k])[-1] > thresh_line:
                        count = count + 1
                        count_his[k] = True

        return count, count_his

    def draw_text(self, img, text,
          font=cv2.FONT_HERSHEY_SIMPLEX,
          pos=(20, 20),
          font_scale=1,
          font_thickness=2,
          text_color=(255, 255, 255),
          text_color_bg=(255,0, 0)):

          

            x, y = pos
            text_size, _ = cv2.getTextSize(text, font, font_scale, font_thickness)
            text_w, text_h = text_size
            cv2.rectangle(img, (15,15), (x + text_w-60, y + text_h+5), text_color_bg, -1)
            cv2.putText(img, text, (x, y + text_h + font_scale - 1), font, 0.80, text_color, font_thickness)

            return text_size

        




