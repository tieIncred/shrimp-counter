import cv2
from tracker import *
from counter import Counter

cap = cv2. VideoCapture('Counting_Shrimps.mp4')
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
   
size = (frame_width, frame_height)
result = cv2.VideoWriter('result_shrimp.avi', 
                         cv2.VideoWriter_fourcc(*'MJPG'),
                         10, size)
tracker = EuclideanDistTracker()
counter = Counter()
object_detector = cv2.createBackgroundSubtractorMOG2()
shrimp_dict = {}
count_his = {}
count = 0
while(cap. isOpened()):
  ret, frame = cap.read()
  frame_test = frame.copy()
  gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  blurred = cv2.GaussianBlur(gray, (7, 7), 0)
  # thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 21, 10)
  ret, thresh = cv2.threshold(blurred, 200, 255, cv2.THRESH_BINARY)
  # cv2.imshow('frame', thresh)
  # mask = object_detector.apply(thresh)
  cv2.imshow('frame', thresh)
  contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  detections = []
  for cnt in contours:
      area = cv2.contourArea(cnt)
      if area > 500:
        #   cv2.drawContours(frame, [cnt], -1, (0, 255, 0), 2)
        x, y, w, h = cv2.boundingRect(cnt)
        # cv2.rectangle(frame_test, (x, y), (x+w, y+h), (0,255,0), 3)
        detections.append([x,y,w,h])

      
  #tracking and Counting
  boxes_ids = tracker.update(detections)
  shrimp_dict = counter.register_object(shrimp_dict, boxes_ids)
  count, count_his = counter.countShrimps(shrimp_dict, 540, count)
  for box_id in boxes_ids:
        x, y, w, h, id = box_id
        cv2.putText(frame_test, str(id), (x, y - 15), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
        cv2.rectangle(frame_test, (x, y), (x + w, y + h), (0, 255, 0), 3)
        cv2.line(frame_test, (0,540), (1920,540),color = (0, 0, 255),thickness = 3)
        count_text = 'Shrimp Counter : ' + str(count)
        counter.draw_text(frame_test, count_text)
  # cv2.imshow('frame', frame_test)
  result.write(frame_test)
  key = cv2.waitKey(30)

  if key == 27:
      break

cap.release()
cv2.destroyAllWindows()
  
